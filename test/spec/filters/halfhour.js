'use strict';

describe('Filter: halfhour', function () {

  // load the filter's module
  beforeEach(module('eeStecOlympicsFrontendApp'));

  // initialize a new instance of the filter before each test
  var halfhour;
  beforeEach(inject(function ($filter) {
    halfhour = $filter('halfhour');
  }));

  it('should return the input prefixed with "halfhour filter:"', function () {
    var text = 'angularjs';
    expect(halfhour(text)).toBe('halfhour filter: ' + text);
  });

});
