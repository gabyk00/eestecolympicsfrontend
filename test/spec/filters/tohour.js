'use strict';

describe('Filter: toHour', function () {

  // load the filter's module
  beforeEach(module('eeStecOlympicsFrontendApp'));

  // initialize a new instance of the filter before each test
  var toHour;
  beforeEach(inject(function ($filter) {
    toHour = $filter('toHour');
  }));

  it('should return the input prefixed with "toHour filter:"', function () {
    var text = 'angularjs';
    expect(toHour(text)).toBe('toHour filter: ' + text);
  });

});
