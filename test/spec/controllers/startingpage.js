'use strict';

describe('Controller: StartingpageCtrl', function () {

  // load the controller's module
  beforeEach(module('eeStecOlympicsFrontendApp'));

  var StartingpageCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    StartingpageCtrl = $controller('StartingpageCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
