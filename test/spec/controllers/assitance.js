'use strict';

describe('Controller: AssitanceCtrl', function () {

  // load the controller's module
  beforeEach(module('eeStecOlympicsFrontendApp'));

  var AssitanceCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    AssitanceCtrl = $controller('AssitanceCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
