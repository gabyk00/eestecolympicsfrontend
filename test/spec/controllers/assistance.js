'use strict';

describe('Controller: AssistanceCtrl', function () {

  // load the controller's module
  beforeEach(module('eeStecOlympicsFrontendApp'));

  var AssistanceCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    AssistanceCtrl = $controller('AssistanceCtrl', {
      $scope: scope
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(scope.awesomeThings.length).toBe(3);
  });
});
