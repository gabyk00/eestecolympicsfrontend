'use strict';

describe('Service: AssistanceService', function () {

  // load the service's module
  beforeEach(module('eeStecOlympicsFrontendApp'));

  // instantiate service
  var AssistanceService;
  beforeEach(inject(function (_AssistanceService_) {
    AssistanceService = _AssistanceService_;
  }));

  it('should do something', function () {
    expect(!!AssistanceService).toBe(true);
  });

});
