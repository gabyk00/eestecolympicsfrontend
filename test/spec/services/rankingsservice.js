'use strict';

describe('Service: RankingsService', function () {

  // load the service's module
  beforeEach(module('eeStecOlympicsFrontendApp'));

  // instantiate service
  var RankingsService;
  beforeEach(inject(function (_RankingsService_) {
    RankingsService = _RankingsService_;
  }));

  it('should do something', function () {
    expect(!!RankingsService).toBe(true);
  });

});
