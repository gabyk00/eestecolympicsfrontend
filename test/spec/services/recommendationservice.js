'use strict';

describe('Service: RecommendationService', function () {

  // load the service's module
  beforeEach(module('eeStecOlympicsFrontendApp'));

  // instantiate service
  var RecommendationService;
  beforeEach(inject(function (_RecommendationService_) {
    RecommendationService = _RecommendationService_;
  }));

  it('should do something', function () {
    expect(!!RecommendationService).toBe(true);
  });

});
