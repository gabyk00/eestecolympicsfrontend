'use strict';

describe('Service: SERVER', function () {

  // load the service's module
  beforeEach(module('eeStecOlympicsFrontendApp'));

  // instantiate service
  var SERVER;
  beforeEach(inject(function (_SERVER_) {
    SERVER = _SERVER_;
  }));

  it('should do something', function () {
    expect(!!SERVER).toBe(true);
  });

});
