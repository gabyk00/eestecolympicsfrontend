'use strict';

describe('Service: BookingService', function () {

  // load the service's module
  beforeEach(module('eeStecOlympicsFrontendApp'));

  // instantiate service
  var BookingService;
  beforeEach(inject(function (_BookingService_) {
    BookingService = _BookingService_;
  }));

  it('should do something', function () {
    expect(!!BookingService).toBe(true);
  });

});
