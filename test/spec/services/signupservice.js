'use strict';

describe('Service: SignUpService', function () {

  // load the service's module
  beforeEach(module('eeStecOlympicsFrontendApp'));

  // instantiate service
  var SignUpService;
  beforeEach(inject(function (_SignUpService_) {
    SignUpService = _SignUpService_;
  }));

  it('should do something', function () {
    expect(!!SignUpService).toBe(true);
  });

});
