'use strict';

describe('Directive: onedayschedule', function () {

  // load the directive's module
  beforeEach(module('eeStecOlympicsFrontendApp'));

  var element,
    scope;

  beforeEach(inject(function ($rootScope) {
    scope = $rootScope.$new();
  }));

  it('should make hidden element visible', inject(function ($compile) {
    element = angular.element('<onedayschedule></onedayschedule>');
    element = $compile(element)(scope);
    expect(element.text()).toBe('this is the onedayschedule directive');
  }));
});
