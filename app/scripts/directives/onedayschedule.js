'use strict';

/**
 * @ngdoc directive
 * @name eeStecOlympicsFrontendApp.directive:onedayschedule
 * @description
 * # onedayschedule
 */
angular.module('eeStecOlympicsFrontendApp')
  .directive('onedayschedule', function () {
    return {
      scope: {
        slots: '=ngSlots',
        schedule: '=ngSchedule'
      },
      templateUrl: 'views/onedayschedule.html',
      restrict: 'E',
      link: function postLink(scope, element, attrs) {
      	var _selection = {
      		state: false,
      		//unit represents half an hour
      		unit: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
      	};

      	var _slots = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];

      	function _loop(begin, end, step) {
          var array = [];
          
          for (var i = begin; i <= end; i += step) {
            array.push(i);
          }
          
          return array;
        }

        function _toggle(hour) {
          if (_selection.state) {
            if (!scope.schedule[hour].isOccupied) {            
              // scope.slots[hour] = !scope.slots[hour];
              scope.slots[hour] = true;
            } else {
              if (!isSolo(scope.slots, hour)) {
                scope.$emit('errorOnSlotSelect');
              } else {
                scope.$emit('bookingClicked', hour);
              }
            }
          }
        }

        function _select(state, hour) {
          _selection.state = state;

          if (!slotsAreEmpty(scope.slots)) {
            _selection.state = false;
            if (state)
              scope.$emit('resolveBookingError');
          }

          if (_selection.state) {
            _toggle(hour);
          }
        }

				scope.onMouseLeave = function() {
          _selection.state = false;
        };        

        function _init() {
          scope.loop = _loop;
          scope.toggle = _toggle;
          scope.select = _select;
          
          // scope.slots = _slots;
        }
        
        _init();        		

        scope.show = function(string) {
          console.log(string)
        }

        scope.seeBooking = function() {
          console.log(scope.slots)
          for (var i = 0; i < 48; ++i) {
            if (scope.slots[i]) {
              break;
            }
          }

          if (i == 48) return;

          var j = i;
          while (j < 48 && scope.slots[j]) ++j;

          scope.$emit('bookingSelected', {
            from: i,
            until: j
          });
        };
      }
    };
  });

function slotsAreEmpty(slots) {
  for (var i = 0; i < slots.length; ++i) {
    if (slots[i]) {
      return false;
    }
  }
  return true;
};

function isSolo(slots, index) {
  return !slots[index-1] && !slots[index+1];
};