'use strict';

/**
 * @ngdoc service
 * @name eeStecOlympicsFrontendApp.DateService
 * @description
 * # DateService
 * Factory in the eeStecOlympicsFrontendApp.
 */
angular.module('eeStecOlympicsFrontendApp')
  .factory('DateService', function () {
    // Service logic
    // ...

    // Public API here
    return {
      parseDate: function (date) {
        return date.getDate() + '-' + (date.getMonth() + 1) + '-' + date.getFullYear();
      }
    };
  });
