'use strict';

/**
 * @ngdoc service
 * @name eeStecOlympicsFrontendApp.RankingsService
 * @description
 * # RankingsService
 * Factory in the eeStecOlympicsFrontendApp.
 */
angular.module('eeStecOlympicsFrontendApp')
  .factory('RankingsService',
    ['$q',
    '$http',
    'SERVER',

    function ($q, $http, SERVER) {
    // Service logic
    // ...

    // Public API here
    return {
      getRankings: function () {
        var deferred = $q.defer();

        $http.get(SERVER.URL + SERVER.RANKINGS)
        .success(function(data) {
          deferred.resolve(data);
        }).error(function(data) {
          deferred.reject(data);
        });
        return deferred.promise;
      }
    };
  }
]);
