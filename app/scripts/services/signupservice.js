'use strict';

/**
 * @ngdoc service
 * @name eeStecOlympicsFrontendApp.SignUpService
 * @description
 * # SignUpService
 * Factory in the eeStecOlympicsFrontendApp.
 */
angular.module('eeStecOlympicsFrontendApp')
  .factory('SignUpService',
    ['$q',
    '$http',
    'SERVER',

    function ($q, $http, SERVER) {
    // Service logic
    // ...


    // Public API here
      return {
        createSignUpObject: function (fields) {
          return {
            username : fields[0].answer,
            password : fields[1].answer,
            firstName: fields[2].answer,
            lastName : fields[3].answer,
            email    : fields[4].answer
          };
        },
        signUp: function(credentials) {
          var deferred = $q.defer();
          
          $http.post(SERVER.URL + SERVER.SIGN_UP, credentials)
            .success(function(data, status, headers, config) {
              deferred.resolve(data);
            })
            .error(function(data) {
              deferred.reject(data);
            });
          return deferred.promise;
        }

      };
  }
]);
