'use strict';

/**
 * @ngdoc service
 * @name eeStecOlympicsFrontendApp.SERVER
 * @description
 * # SERVER
 * Constant in the eeStecOlympicsFrontendApp.
 */
angular.module('eeStecOlympicsFrontendApp')
  .constant('SERVER', {
  	URL: 'http://172.20.24.174:1337',
  	SIGN_UP: '/signup',
  	LOG_IN: '/login',
  	LOG_OUT: '/logout',
  	USER_INFO: '/userinfo',
  	CALENDAR: '/calendar',
  	NEW_TASK: '/newtask',
  	ASSISTANT: '/assistant',
  	PREFERENCES: '/preferences',
    RANKINGS: '/clasament'
 });
