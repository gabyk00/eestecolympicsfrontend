'use strict';

/**
 * @ngdoc function
 * @name eeStecOlympicsFrontendApp.controller:SignupCtrl
 * @description
 * # SignupCtrl
 * Controller of the eeStecOlympicsFrontendApp
 */
angular.module('eeStecOlympicsFrontendApp')
  .controller('SignupCtrl',
  	['$scope',
    '$state',
    'SignUpService',

  	function ($scope, $state, SignUpService) {
    	$scope.fields = [{
    		text: 'First Name',
    		answer: ''
    	}, {
    		text: 'Last Name',
    		answer: ''
    	}, {
    		text: 'Username',
    		answer: ''
    	}, {
    		text: 'Password',
    		answer: ''
    	}, {
    		text: 'Email',
    		answer: ''
    	}];

    	$scope.signup = function() {
        var signUpObject = SignUpService.createSignUpObject($scope.fields);
        SignUpService.signUp(signUpObject)
        .then(function() {
          $state.go('startingPage');
        }, function(error) {
          alert(error);
        });
    	};
  	}
]);
