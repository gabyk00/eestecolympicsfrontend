'use strict';

/**
 * @ngdoc function
 * @name eeStecOlympicsFrontendApp.controller:RankingsCtrl
 * @description
 * # RankingsCtrl
 * Controller of the eeStecOlympicsFrontendApp
 */
angular.module('eeStecOlympicsFrontendApp')
  .controller('RankingsCtrl',
  	['$scope',
  	'RankingsService',

  	function ($scope, RankingsService) {
  		$scope.rankings = [];

  		RankingsService.getRankings()
  		.then(function(data) {
  			$scope.rankings = data;
  			console.log(data)
  		});
  	}
 ]);
