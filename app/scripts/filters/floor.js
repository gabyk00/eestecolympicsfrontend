'use strict';

/**
 * @ngdoc filter
 * @name eeStecOlympicsFrontendApp.filter:floor
 * @function
 * @description
 * # floor
 * Filter in the eeStecOlympicsFrontendApp.
 */
angular.module('eeStecOlympicsFrontendApp')
  .filter('floor', function () {
    return function (input) {
      return Math.floor(input);
    };
  });
