'use strict';

/**
 * @ngdoc filter
 * @name eeStecOlympicsFrontendApp.filter:halfhour
 * @function
 * @description
 * # halfhour
 * Filter in the eeStecOlympicsFrontendApp.
 */
angular.module('eeStecOlympicsFrontendApp')
  .filter('halfhour', function () {
    return function (input) {
      return (input % 2) * 3;
    };
  });
